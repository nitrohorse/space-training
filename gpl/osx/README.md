# Mac Binaries

* `gpl_mac_64bit` was compiled using GNU g++. You need the GNU libraries for it to run.

* `gpl_mac_64bit_compiled_with_Apple_compiler` was compiled using Apple's c++ compiler.  It should run without any GNU libraries.
