# GPL interpretor binary files
1. Download the correct file for your architecture.
2. Give the file execute permission:
    * `$ chmod 700 gpl`
3. If the gpl file is in the same directory the game, you can run the game like this:
    * `$ gpl space_training.gpl`
    If that does not work, try this:
    * `$ ./gpl space_training.gpl`
