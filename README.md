[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://github.com/trance-with-me/Space-Training/blob/master/LICENSE)

# Space Training

A game created using my awesome professor's game programming language (GPL) for my 'Compiler Design' class at university.

### About:

Titled "Space Training," but aptly code-named "Unicorns vs Yetis" for no meaningful reason whatsoever, my game puts the player in the role of Astro, the pale-skinned space explorer cadet. Dodge the meteorites which spawn incrementally the higher the player's score goes. Go see Yorp, the green alien, and he'll give you some advice. Try to beat my high score of 18,000 points, and Astro will finally complete his space training and 
become commander of all the spaceships! (You'll also meet some<i>thing</i> very special)

## To play: 
* Download and follow the setup instructions for the GPL interpreter for Linux and OSX [here](http://www.ecst.csuchico.edu/~tyson/) (look for `CSCI 515 Compiler Design`). Or download the binary from the [gpl directory](https://github.com/nitrohorse/Space-Training/tree/master/gpl).
* Install freeglut3: `sudo apt-get install freeglut3`
* Run: `./gpl space_training.gpl`

### Controls:
Left & Right arrow keys = Movement
<br>Up arrow key = Jump/Fly

***

![Alt text](http://i.imgur.com/aEjuKqm.png)

***

#### Thanks:
Special thanks to [Jason Dietrich](https://github.com/jrd730) for helping me implement gravity and to [Scott LaVigne](https://github.com/pyrated) for helping me implement friction, an impulse on Astro when hitting a meteorite, fixing the high score counter, and Astro's run animation.
